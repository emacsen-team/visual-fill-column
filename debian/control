Source: visual-fill-column
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 13)
             , dh-elpa
Rules-Requires-Root: no
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/emacsen-team/visual-fill-column
Vcs-Git: https://salsa.debian.org/emacsen-team/visual-fill-column.git
Homepage: https://github.com/joostkremers/visual-fill-column/
Testsuite: autopkgtest-pkg-elpa

Package: elpa-visual-fill-column
Architecture: all
Depends: ${elpa:Depends}
       , ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: Emacs mode that wraps visual-line-mode buffers at fill-column
 Visual-fill-column-mode is a small Emacs minor mode that mimics the
 effect of fill-column in visual-line-mode.  Instead of wrapping
 lines at the window edge, which is the standard behaviour of
 visual-line-mode, it wraps lines at fill-column.  If fill-column
 is too large for the window, the text is wrapped at the window edge.
